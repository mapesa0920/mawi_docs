Supervisar agentes
******************

Supervisar agentes es una sección creada para monitorear los estados de un agente mientras este 
se encuentre conectado o no. Como se muestra a continuación, se moestrará las atenciones en 
proceso que tiene el agente, las atenciones terminadas, la fecha y hora de la ultima conexción 
a la app y el tiempo de uso o si se encuentra desconectado.

Para ingresar a *Supervisar agentes*, debe dirigirse al menú **Supervisión**-*Supervisar agentes*.

.. image:: images/supervision/supervisar_agentes1.PNG
        :align: center

Chat-Spy
********

Chat-spy es uan herramienta de supervisión que nos permite monitorear en tiempo real las 
conversaciónes o atenciones que el agente tiene abiertas. es decir, que podemos ver los 
mensajes que se intercambia el agente y el cliente en tiempo real. Esto es por cada atención.

Como usar Chat-spy
==================

1. Se escoge al agente que se desea espiar.

.. image:: images/supervision/chat_spy1.png
        :align: center

Se desplegarán las atenciones que el agente tiene abiertas con el detalle de las mismas 
como se muestra a continuación. 

.. image:: images/supervision/chat_spy2.png
        :align: center

2. Se da click en el botón *Ver mensajes*. 

Se desplegará la antención con los mensajes de este. 

.. note:: 

        El supervisor no podrá intervenir directamente en la conversación.

.. image:: images/supervision/chat_spy3.png
        :align: center

*Figura 1: Vista del chat del agente*

.. image:: images/supervision/chat_spy4.png
        :align: center

*Figura 2: Vista del supervisor al chat del agente*

Cola y atenciones
*****************

La herramienta de *cola y atenciones* es muy útil a la hora de priorizar atenciones de clientes 
preferenciales o de distribuir carga de atenciones entre los agentes. es decir, esta nos 
permite asignar o reasignar atenciones entre los agentes a apreferencia del supervisor, ya sea 
porque un cliente debe ser atendido por un agente en específico o porque un agente tiene más 
atenciones asignadas que otro. 

Existen dos *cajas informativas* que nos muestran las atenciones *En cola* y atenciones *En proceso* 
estas nos brindan información general de esas atenciones; y por lo tanto son las que pueden ser 
asignadas o reasignadas. 

Como usar Cola y atenciones
===========================

Para asignar las atenciones *En cola* o que no están siendo atendidas por algún agente nos dirigimos 
a la tabla de *Cola de chat*, en la cual se mostrarán las atenciones disponibles para ello. 

.. image:: images/supervision/colas1.png
        :align: center

1. Se selecciona al agente al que se quiera asignar la atención. 

2. Click en *Asignar*. 

Para reasignar una atención que ya tenga un agente, nos diriguimos a la tabla de 
*Atenciones en proceso*, en la cual se moestrará que agente tiene dicha atención y al 
cual se quiere asignar. 

.. image:: images/supervision/colas2.png
        :align: center

1. Se selecciona al agente al que se quiera reasignar la atención. 

2. Click en *Reasignar*. 
