Como generar un reporte de transferencia
****************************************

En el menú de **Reportes** - *Transferencia* seguimos los siguientes pasos:

.. image:: images/reporte_transferencia/1.png
        :align: center

1. Se selecciona o no el agente para filtrado. 

.. image:: images/reporte_transferencia/2.png
        :align: center

2. Se selecciona el interlavo de fecha de interes. 

.. image:: images/reporte_transferencia/3.png
        :align: center

3. Puede usar el numéro de usuario si lo tiene. 

4. click en *Buscar*.

Como descargar el reporte del transferencia
*******************************************

Puede descargar dos tipos de formato .xlsx (excel) o .csv

.. image:: images/reporte_transferencia/4.png
        :align: center

5. Click en el formato de preferencia. 