*******************
INTRODUCCION A MAWI
*******************

`Mawi <https://www.mawi.chat/>`_  es una solución de Software en nube destinada a soportar la gestión, operación y administración de una línea de Whatsapp por un grupo de agentes.

.. image:: images/what_is.png
        :align: center

*Figura 1: Mawi sitio web*

Esta herramienta te permite centralizar en una sola consola los chats de Whatsapp, aumentando la productividad de sus agentes y la contactabilidad de sus clientes, todo en una única plataforma pensada para la colaboración de su equipo.

Con `Mawi <https://www.mawi.chat/>`_, tendrás la posibilidad de responder en tiempo real a las diferentes solicitudes, independientemente de su procedencia, y brindar atención instantánea a tus clientes directamente desde Whatsapp.

Si deseas saber más acerca de como comenzar a usar `Mawi <https://www.mawi.chat/>`_ dale click `aquí <https://api.whatsapp.com/send?phone=573128769728&text=Hola%20%F0%9F%99%82%2C%20quisiera%20saber%20sobre%20Mawi>`_


MANUAL DE SUPERVISOR
********************
En esta sección se repasan las acciones que los supervisores de Mawi pueden realizar dentro de la app. Algunas de estas acciones pueden ser, desde hacer 
reporteria de los chats o atenciones, como de supervisar en tiempo real las conversaciones de los agentes con los clientes.

.. toctree::
  :maxdepth: 1

  manual_supervisor.rst



MANUAL DE AGENTE
****************
En esta sección se repasan todas las acciones que un agente de Mawi pueda realizar dentro de una operación. Cuestiones que van desde la atención/generación de
una chats o atenciones, gestión de reportes, transferencias de chats y mucho más, son tratadas en este capítulo de la documentación.

.. toctree::
  :maxdepth: 1

  manual_agente.rst


NOTAS DE VERSIÓN
****************
En esta sección abordaremos las notas relacionadas con las actializaciones de las diferentes versiones de Mawi.

.. toctree::
  :maxdepth: 1

  release_notes.rst
