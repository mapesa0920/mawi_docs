Integración fomularios-calificaciones-campañas
**********************************************

Para gestionar nuestras campañas tenemos que tener en cuenta que estás deben tener una 
calificación y un formulario asociados, de la siguiente forma:

.. image:: images/gestion1.png
        :align: center

*Figura 1: Contenencia de las campañas*

Como se muestra en la *Figura 1* las los formatos y las calificaciones están ligadas o 
contenidas dentro de las campañas, por lo que para modificar el formulario de la 
calificación hay que crear un nuevo formulario y editar la calificación de la campaña 
cambiando el formulario. 

.. note:: 

    No es posible directamente cambiar el formulario de la calificación por 
    integridad de la información antigua. 

.. image:: images/gestion.PNG
        :align: center

*Figura 2: Menú de gestiones*

Formularios
***********

Los formularios de campaña constituyen el método por defecto para recolectar información 
(relevante para la campaña) en la interacción con la persona detrás de una comunicación 
establecida. Son diseñados por el supervisor combinando en una vista estática diferentes 
tipos campos (texto, fecha, de multiple selección y área de texto), pudiendo ordenar 
los mismos de acuerdo a la necesidad de la campaña.

Los formularios pueden contener campos del tipo:

- **Texto**
- **Fecha**
- **Combo de selección múltiple**

.. toctree::

    gestion_formularios.rst

Calificaciones
**************

Las calificaciones constituyen un listado de "etiquetas" disponibles para ser asignadas 
a cualquier campaña, de manera tal que los agentes puedan clasificar cada atención
dentro de una campaña, con cualquiera de éstas calificaciones.

De lo anterior, se puede entender que para cada diferente calificación hay que asociar 
un formularios. Es decir, un agente puede elegir entre distinlas calificaciones 
(con distintos formatos) a la hora de cerrar la atención con un cliente. 

.. toctree::

    gestion_calificaciones.rst

Campañas
********

En un Contact Center, una campaña es un conjunto de acciones y recursos tanto técnicos 
como humanos, que se aplican para conseguir un determinado objetivo de negocio y de 
interacción con los clientes y prospectos.

Las campañas pueden ser de muchos tipos telemarketing, de atención al cliente, soporte, 
promociones entre otras, en mawi las campañas hacen referencia al grupo de agentes que 
atenderán las interacciones, la calificación y el formulario de cierre que utilizarán dichos 
agentes.

.. toctree::

    gestion_campanas.rst

Respuestas rápidas
******************

Las respuestas rápidas son la forma de agilizar y acortar los tiempos de atención 
evitando escribir textos largos.


.. toctree::

    gestion_respuestas.rst

