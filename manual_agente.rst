****************
Manual de agente
****************

Vamos a dividir el manual de gestión del agente en los diferentes tópicos citados debajo.

.. image:: images/consola.png
        :align: center

*Figura 1: Consola o vista de agente*


Login
=====

Para ingresar el app es necesario que se contacte con el supervisor y que este le asigne un usuario. 

.. image:: images/agentes/login.PNG
        :align: center

.. note::

        Recuerde que despues de tres intentos fallidos de contraseñas el usuario se bloqueará. 
        Notifique al supervisor en caso de ocurrir.


Chats contestados
=================

Los chats contestados se dispondrán en la parte izquiera de la pantalla de agente en 
**Contestados**. Aquí apareceran todos los mensajes que hayan sido tomados por 
el agente, los que se les hayan sido asignados por el supervisor o los que les hayan 
sido transferidos por un agente.

.. image:: images/agentes/nuevos_contestados.png
        :align: center

Nuevos mensajes
===============

Los nuevos mensajes se muestran en la bandeja de **Nuevos**, y cuando lleguen mensajes nuevos 
se notidicafá por medio del navegador y se mostrará un acumulador verde en el título de la bandeja.

.. image:: images/agentes/nuevos_mensajes.png
        :align: center

Nuevos a contactos
------------------ 

Para escribir a un contacto que ya se encuentre guardado en la agenda de contactos o a un cliente 
del cual sepamos su número, debemos ingresar o darle click al globo de chat que se muestra a 
continuación. 

.. image:: images/agentes/contactos.PNG
        :align: center

Aquí se desplegará un menú que nos permiritá elegir un contacto o escribir su número.

.. image:: images/agentes/contactos2.PNG
        :align: center

1. Escribir a un nuevo contacto escogiendo el indicativo y el número del mismo. 

2. Escoger un contacto que esté ya guardado. 


Transferencias
==============

Para la transferencia de atenciones o chats a otro agente se procede de la siguente forma.

.. image:: images/agentes/transferencias.png
        :align: center

1. Se da click al ícono de *doble fecha* que se encuentra dentro del chat a transferir en la 
parte superior derecha. 

2. Se escoge el agente al cual quiere transferir. 

3. Se da click al botón *Transferir* 

.. note:: 

        Recuerde que todas las transferencias serán registradas y notificadas al supervisor en 
        sus respectivos reportes. 

Adjuntar archivos
=================

Parte importante a la hora de la gestión diaria entre un agente entre un agente y el cliente es 
el envio de archivos. estos pueden ser del tipo: 

- Tipo texto (PDF, .docs, .xlsx, .csv, ect.)

- Imagenes

- Ubiciación

- Vcards

Por eso, para enviar este tipo de archivos se debe: 

.. image:: images/agentes/adjuntar.PNG
        :align: center

1. Dar click en el ícono del *clip* y seleccionar el tipo de archivo o información que desee enviar.

2. Escoger el tipo de archivos. 

3. Escoger en su dispositivo el respectivo a enviar.

.. note:: 

        Tambien puede arrastrar al chat abierto el archivo que quiere enviar. 

.. note:: 

        Recuerde que una vez enviado un archivo no se puede eliminar el mensaje. 

.. note:: 

        Los archivos de tipo vídeo no se permiirán enviar. 

Respuestas rápidas
==================

Las respuestas rápidas son una forma de agilizar y disminuir el tiempo en el que agante 
escribe mensajes repetitivos, por lo que es conveniente usar este tipo de herramientas. 

Para usar las respuestas rapidas basta con escribir el simbolo */* (slash) en la barra de 
escritura en los chats abiertos y buscar el comando de preferencia o que necesite, en la 
barra que aparecerá arriba. O bien puede usar */* seguido del atajo que previamente el 
supervisor le creó para ello. 

.. image:: images/agentes/respuestas.PNG
        :align: center 

1. Usar */*.

2. Escoger el comando de preferencia con un click.

.. image:: images/agentes/respuestas2.PNG
        :align: center

3. O escribir */atajo* y presinar *Enter*.

Cerrar y guardar gestión
========================

Al momento de cerrar una atención o conversación con un cliente, muchas es necesario 
guardar información suministrada por el mismo durante la conversación. 

Para guardar dicha información del cliente, hay que seguir los siguentes pasos:

.. image:: images/agentes/cerrar.PNG
        :align: center

1. Click en el ícono del *diskette* en la parte superior derecha del chat abierto. 

.. image:: images/agentes/cerrar2.PNG
        :align: center

2. Elegir la calificación a la que pertenece la información del cliente a guardar. 

3. Click en el botón *Enviar*.

.. image:: images/agentes/cerrar3.PNG
        :align: center

4. Llenar los diferentes campos correspondientes en el formulario.

5. Click en el botón *Guardar*. 

Logout
======

Para cerrar la sesión del agente se debe dar click en el ícono de *flecha hacia la derecha* 
que se encuentra al aldo de del globo de contactos. Aquí:

.. image:: images/agentes/logout.PNG
        :align: center