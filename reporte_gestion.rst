Como generar un reporte de gestión
**********************************

En el menú de **Reportes** - *Gestión* seguimos los siguientes pasos:

.. image:: images/reporte_gestion/1.png
        :align: center

1. Escoja el formulario que desee. 

.. image:: images/reporte_gestion/2.png
        :align: center

2. Seleccione intervalo de fechas para el reporte. 

3. click en *Buscar*.

Como descargar el reporte del gestión
*************************************

Puede descargar dos tipos de formato .xlsx (excel) o .csv

.. image:: images/reporte_gestion/3.png
        :align: center

4. Click en el formato de preferencia. 