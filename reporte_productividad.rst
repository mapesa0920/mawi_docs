Como generar un reporte de productividad
========================================

En el menú de **Reportes** - *Productividad* seguimos los siguientes pasos:

.. image:: images/reporte_productividad/1.png
        :align: center

1. Seleccionar intervalo de fecha para el reporte. 

.. image:: images/reporte_productividad/2.png
        :align: center

2. Seleccione agente pata repote. 

3. click en *Buscar*.

Como descargar el reporte de productividad
==========================================

Puede descargar dos tipos de formato .xlsx (excel) o .csv

.. image:: images/reporte_productividad/3.png
        :align: center

4. Click en el formato de preferencia. 