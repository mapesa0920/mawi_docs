Como generar un reporte de historial
====================================

En el menú de **Reportes** - *Línea de Tiempo* seguimos los siguientes pasos:

.. image:: images/reporte_timeline/1.png
        :align: center

1. Seleccione pais (para el código).

.. image:: images/reporte_timeline/2.png
        :align: center

2. Número de usuario o número de atención.

3. Click en el botón *Buscar cliente*


