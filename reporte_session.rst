Como generar un reporte de sesión
=================================

En el menú de **Reportes** - *Sessión* seguimos los siguientes pasos:

.. image:: images/reporte_session/1.png
        :align: center

1. Se escoge el intevalo de fecha en el que desea el reporte.

2. click en *Buscar*.

Como descargar el reporte de sesión
===================================

Puede descargar dos tipos de formato .xlsx (excel) o .csv

.. image:: images/reporte_session/2.png
        :align: center

3. Click en el formato de preferencia. 