Como importar contactos
***********************

Para importar los contactos se puede usar dos tipos de archivos. Los tipo .xlsx (excel) o .csv. 
Por lo que en cada uno se deben tener en cuanta lo siguiente: 

.. image:: images/contactos.PNG
        :align: center

1. Ir al menú en **Agenda** - *contactos* 

2. Dar click en el menú en la parte superior derecha. 

3. Dar click en en *importar de*.

.. image:: images/contactos2.PNG
        :align: center

4. Escoger país.

5. Carle click en *Choose File* 

6. Escoger archivo en su dispositivo. 

7. Dar click en cargar datos.

.. note::
        
        Renga en cuenta el tipo de archivo con el que va a importar los contactos.

Tipo excel
========== 

Para importar los contactos en un archivo tipo excel las columnas a tener en cuanta son 
las siguientes:

+------------+-----------+----------+-----+-------------------+-----------------+
|    Número  |  Nombre   | Apellido | Web |      Email        |    Dirección    |
+------------+-----------+----------+-----+-------------------+-----------------+
| 3123456788 | Contacto1 |  Prueba  | *** | prueba@gmail.com  | Calle 1 # 2 - 3 |
+------------+-----------+----------+-----+-------------------+-----------------+
| 3123456789 | Contacto2 |  Prueba2 | *** | prueba1@gmail.com | Calle 1 # 2 - 4 |
+------------+-----------+----------+-----+-------------------+-----------------+

Tipo csv
========

Para importar los contactos en un archivo tipo csv las columnas a tener en cuanta son 
las siguientes:

+------------+-----------+
|    Nombre  |  Número   |
+------------+-----------+
| 3123456788 | Contacto1 |
+------------+-----------+
| 3123456789 | Contacto2 |
+------------+-----------+


Como exportar contactos
***********************

Dar click en la parte superior derecha en la opción *Exportar A* y escoger el tipo 
de archivo deseado.