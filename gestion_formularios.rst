Como crear  un formulario
=========================

En el menú de **Gestión** - *Formularios* seguimos los siguientes pasos:

.. image:: images/crear_formularios/1.PNG
        :align: center

1. Nuevo formulario.

.. image:: images/crear_formularios/2.PNG
        :align: center

2. Se coloca nombre del formulario. 

.. image:: images/crear_formularios/3.png
        :align: center

3. Se agregan los campos requeridos en los formularios y nombre de los mismos. 

4. Se elige el tipo de campo requerido. 

.. note:: 

        Si el campo es de tipo lista tambien deberá añadir los items del mismo.

.. note:: 

        Estos pueden o no ser requeridos al momento de cerrar 
        la atención por parte del agente siempre y cuando le selecionemos 
        el campo requerido.

5. Guardar campo selecionado. 

6. Campos creados. 

7. Guardar formularios en el botón *Finalizar*. 

.. image:: images/crear_formularios/4.PNG
        :align: center

8. Se activa el formulario.













