Como crear un usuario
*********************

Pasos para crear un nuevo usuario:

.. image:: images/crear_usuario/1.PNG
        :align: center

1. Darle click en nuevo usuario.

.. image:: images/crear_usuario/2.PNG
        :align: center

2. Agregar nombre de usuario.

3. Agregar correo usuario. 

.. note:: 

        Recuerde que este es el *usser* para el logín del usuario a crear.

4. Agregar número de contacto celular junto con el respectivo indicativo.

5. Correo de contacto del usuario.

6. Rol del nuevo usuario. 

.. note:: 

        Recuerde escojer el rol pertinente para el nuevo usuario, ya se supervisor o agente.

7. Estado del nuevo usuario. 

8. Contraseña y verificaión de la misma.

9. Guardar.





