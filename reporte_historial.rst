Como generar un reporte del historial
*************************************

En el menú de **Reportes** - *Historial* seguimos los siguientes pasos:

.. image:: images/reporte_historial/1.png
        :align: center

1. Selecciones o no un agente para el filtro. 

.. image:: images/reporte_historial/2.png
        :align: center

2. Escoga un intervalo de fecha o fechas en las que quiere ver el reporte. 

.. image:: images/reporte_historial/3.png
        :align: center

3. Puede tambien incluir el usuario si lo tienen a la mano. 

4. click en *Buscar*.

Como descargar el reporte del historial
***************************************

Puede descargar dos tipos de formato .xlsx (excel) o .csv

.. image:: images/reporte_historial/4.png
        :align: center

5. Click en el formato de preferencia. 